
close all;
clc;
clear;

format long;
rvalues = [];
atkcompvalues = [];
BIGSET = ["Note", "Coefficient", "Raw Attack Time", "Synthesized Attack Time", "Atk time Percent Error"];

setset = [];


mainpath = ["HANGAR","ONGIYUNG","PATTUNG","PITU"];
instrucounter = 0;
columncount = 2;
for y=1:length(mainpath);
    rawpath = append(mainpath(y),'/RAW/');
    propath = append(mainpath(y),'/PRO/');
    
    files1 = dir(append(rawpath,'*.wav'));
    files2 = dir(append(propath,'*.wav'));
    
    rawfiles = char({files1.name});
    profiles = char({files2.name});
   
    coercoefftotal = 0;
    counter = 0;
    atkcomptotal = 0;
    zero_cross = 0;
    rawatk = 0;
    proatk = 0;
    atk_time_percenterror = 0;
    
    BIGSET(columncount,1) = "Instrument " + num2str(y);
    columncount = columncount + 1;
    tempset = [];

    xdata = [];
    ydata = [];

    
    for x=1:length(files1)  
        str1 = append(rawpath,erase(rawfiles(x,:)," "));
        str2 = append(propath,erase(profiles(x,:)," "));
        [signal1,Fs] = audioread(str1);
        [signal2,Fs] = audioread(str2);
    
        audio1 = signal1(:,1);
        audio2 = signal2(:,1);
        
        x_coeff = get_corrcoef(audio1,audio2);
        [rawatk, proatk, atk_diff] = get_atk_time_compare(audio1,audio2);
        

        coercoefftotal = coercoefftotal + x_coeff;
        atkcomptotal = atkcomptotal + atk_diff;

        counter = counter + 1;
        atk_time_percenterror = (proatk-rawatk)/rawatk;

        BIGSET(columncount,1) = x;
        BIGSET(columncount,2) = x_coeff;
        BIGSET(columncount,3) = rawatk;
        BIGSET(columncount,4) = proatk;
        BIGSET(columncount,5) = atk_time_percenterror;
        columncount = columncount + 1;

        tempset(x) = atk_time_percenterror;
        
        xdata(x) = x;
        ydata(x) = x_coeff;
        disp(erase(rawfiles(x,:)," ")+"  "+erase(profiles(x,:)," "));
        
        
    end
    avg_coef = coercoefftotal/counter;
    avg_atk_time_dif = atkcomptotal/counter;
    avg_atk_time_raw = rawatk/counter;
    avg_atk_time_pro = proatk/counter;
    
    BIGSET(columncount,1) = "AVERAGE";
    BIGSET(columncount,2) = avg_coef;
    BIGSET(columncount,5) = mean(tempset);
    columncount = columncount + 1;
    BIGSET(columncount,1) = "STANDARD DEVIATION";
    BIGSET(columncount,5) = std(tempset);
    columncount = columncount + 1;
    


    instrucounter = instrucounter + 1;
    

    rvalues(instrucounter) = avg_coef;

    atkvaluescomp(instrucounter,1) = avg_atk_time_raw;
    atkvaluescomp(instrucounter,2) = avg_atk_time_pro;
    atkvaluescomp(instrucounter,3) = avg_atk_time_dif;
    
    figure;
    plot(xdata,ydata);
    ylim([0 1]);


end

% writematrix(BIGSET,"results.xlsx");


function x_coeff = get_corrcoef(x,y)
    spec1 = abs(spectrogram(x));
    spec2 = abs(spectrogram(y));
    r = corrcoef(spec1,spec2);
    x_coeff = r(2,1);
end

function [atk1, atk2, comp] = get_atk_time_compare(x,y) 
    a = get_atk_time(x);
    b = get_atk_time(y);
    atk1 = a;
    atk2 = b;
    comp = abs(a-b);
end


function atktime = get_atk_time(x) 
    x = abs(x(1:floor(0.25*length(x))));
    xmax = max(x(1:floor(0.25*length(x))));
    lower = find(x > 0.1 * xmax,1,"first");
    upper = find(x > 0.9 * xmax,1,"first");
    atktime = (1/44100)*(upper-lower)*1000;
end




