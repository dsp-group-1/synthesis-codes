files = dir('*.wav');     %open all the audio files in directory
filenames = char({files.name});

avgr = 0;

%  this for-loop is for iterating the audio synthesis for all files in a
%  single folder
for x=1:length(files) 

    filetoread = filenames(x,:)
    finalfiletoread = erase(filetoread," ")
    
    
    [m,Fs] = audioread(finalfiletoread); %read the audio file and get sampling frequency


    P = 1/Fs;                   
    duration = length(m)*P;
    t = 0:P:duration-P;
    
    m = m(:,1);                 % convert signal to mono
    m = m/max(abs(m));          % normalize the signal
    
    transform = fft(m,Fs);      % get the fourier transform of the signal
    
    
    % we get 200 peaks from the fourier transform under Fs/2 (Nyquist limit)
    [pks, loc] = findpeaks(abs(transform(1:22050,1)),"SortStr","descend",NPeaks=200);
    [angles] = angle(transform(loc));

    % pks = magnitude,  angles = phase
    
    newsignal = 0 ;
    
    for i=1:length(loc)
        newsignal = newsignal + (pks(i)).*(sin((loc(i)*2*pi).*t+angles(i)));
        % additive synthesis
    end
    
    newsignal = newsignal';
    n = 10;
    % parameter of n-tap hilbert filter used for getting the envelope
    % this parameter is varied from sample to sample to optimize the
    % results
    [yupper,ylower] = envelope(m);    % getting the envelope from the original signal
    lastsignal = newsignal.*yupper;     % applying the envelope to the new signal
    
    
    normlastsignal = lastsignal/max(abs(lastsignal));   % normalize the new signal
    normlastsignal = normlastsignal*max(abs(m));         % bring new signal to the same level as old

    newfilename = strcat(erase(finalfiletoread,'.wav'),'_synthesized.wav') ;   
    audiowrite(newfilename,normlastsignal,Fs); %write new synthesized file

    
    afft = abs(spectrogram(m));
    bfft = abs(spectrogram(normlastsignal));
    r = corrcoef(afft,bfft);
    avgr = avgr + r(2,1);
end

disp(avgr/9);