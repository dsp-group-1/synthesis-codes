files = dir('*.wav');     %open all the audio files in directory
filenames = char({files.name});


for x=1:length(files) 

    close all;
    bestz = 100; %initial value for iterator

    filename = filenames(x,:)
    % counter = str2num(filename(11));
    finalsynthname = filename;
    filename = finalsynthname;
    digitalwaveguide;
    iterator;
    digitalwaveguide;
    newfilename = strcat(erase(filename,'.wav'),'_synthesized.wav');
    audiowrite(newfilename,sound_out,fs); %write new synthesized file
end