
% firstname = 'ongiyung_big_track12.wav';
% filename = ['/Users/laustinrivar/Documents/UPD/5th - Second Sem (2023-2024)/ECE 199/DW Test/', firstname];
% finalsynthname = 'xxxx.wav';

% INITIALIZATION

%Sample the inserted signal 
[signal,fs] = audioread(filename);
signal = signal(:,1);
Ts = 1/fs; % sampling period 
N = length(signal);
duration = N*Ts;
sound(signal, fs);
%FFT
fftSignal = fft(signal); % Perform FFT
fftMagnitude = abs(fftSignal/N); % Magnitude of the FFT
fftMagnitude = fftMagnitude(1:floor(N/2+1)); % Take only the positive frequencies
fftMagnitude(2:end-1) = 2*fftMagnitude(2:end-1); % Adjust the magnitude for positive frequencies

%Find Fundamental Frequency
f = (0:(N/2)) * (fs/N); % Frequency vector
[~, index] = max(fftMagnitude); % Find the peak in the FFT magnitude
freq0 = f(index); % Fundamental Freq

%%
%LOOP GAIN

K = 10; % Harmonics to be extracted
delay_L = round(freq0/1); % divided by 2 by default, set to 1 for some cases

a1dl = delay_L;
nfft = 2^nextpow2(delay_L); % Length of FFT

% Compute Short-time Fourier Transform (STFT)
window = hamming(delay_L); % Hamming window
[S, f, ~] = spectrogram(signal, window, delay_L-nfft/4, nfft, fs); % Compute spectrogram

% Measure magnitude of each harmonic
harmonics = zeros(K, size(S, 2)); % Initialize harmonics matrix
for k = 1:K
    % Find peak magnitude of each harmonic in each frame
    [~, idx] = max(abs(S(round(f(k)*delay_L/fs)+1, :))); % Find index of maximum magnitude
    harmonics(k, :) = abs(S(round(f(k)*delay_L/fs)+1, :)); % Stores the magnitude
end

% Fit straight line on logarithmic amplitude scale
log_harmonics = log10(harmonics); % Convert to log
time_indices = (0:size(log_harmonics, 2)-1) * (nfft/4) / fs; % STFT frames

% Fit a straight line
B = zeros(K, 2); 
for k = 1:K
    B(k, :) = polyfit(time_indices, log_harmonics(k, :), 1); %Linear fit on log scale
end

g = 10.^(B*delay_L/(20*(nfft/4))); % loop gain
%%
% PEAK FINDING

% Compute STFT with blackman window
[S,F,T,~] = spectrogram(signal,blackman(round(fs*0.02)), round(fs*0.02*0.01),nfft,fs,'power','yaxis');
max_freq = max(abs(S),[],2); % Find maximum magnitude in each freq

% Find peaks
[~, pk_locs] = findpeaks(max_freq,'MinPeakHeight', -inf, 'sortstr','descend', 'minpeakprominence',1 ,'npeaks',16);
pk_locs = sort(pk_locs); % Sort locations
pk_freqs = F(pk_locs); % Get peak frequencies

%%
% EXCITATION SIGNAL 

gain = 10.^((delay_L*g(:,1))/(20*nfft)); % Compute gain
H = [1; gain; 1]; % Frequency response
npk_freqs = pi*[0; pk_freqs/(fs/2); 1]; % Normalize peak freqs

% Interpolate to expand pk_locs to match H and W
H_L = length(H); % Get H length
W = linspace(npk_freqs(1), npk_freqs(end), H_L); % Inerpolate
W = min(round(W), npk_freqs(end)); % Round values and make sure max freq is not exceeded

% Extract excitation signal
[b,~] = invfreqz(H,W,3,0); % Find filter coefficient
 den = (0.1)*[zeros(1,round(delay_L)),-b,0] + 0.9*[0,zeros(1,round(delay_L)),-b]; % Construct denominator
 den(1) = 1; % Set first coefficient to 1
 h = impz(1,den); % Impulse response
 x = deconvwnr(signal,h,0); % Deconvolve signal with impulse response

%%
% FRACTIONAL DELAY FILTER

% Parameters
Nn = fs / freq0; % Ideal delay line length
N_int = floor(Nn); % Integer part
N_frac = Nn - N_int; % Fractional part
N_new = 3 * N_int; % Extend delay line length to an integer multiple


% Create extended delay line with integer delay
extendedDelayLine = [zeros(N_new, 1); x]; % Ensure input signal starts after delay
fractionalDelayFilter = [1 - N_frac; N_frac]; % Fractional delay filter 
filteredOutput = filter(fractionalDelayFilter, 1, extendedDelayLine); % Apply
nexcited = filteredOutput(N_new + 1 : N_new + length(x)); % Trim

%%
% DIGITAL WAVEGUIDE MODEL
apattern = fs/freq0;
pause(1)
sound_out = zeros(N, 1);  % Initialize output sound array
excited = nexcited;

Kr = 0.94;
delay_L = bestz;

%Wave array values from left to right 
r_line=zeros(1,delay_L); 
%Wave array values from right to left 
l_line=zeros(1,delay_L); 

for k=1:N 
  
    %Wave travel 
    r_down=r_line(end)*Kr; % ready to go into the lower rail 
    l_down=l_line(1)*Kr; % ready to go into the upper rail 
  
    % rotation 
    r_line=circshift(r_line,[1 1]); 
    l_line=circshift(l_line,[1 -1]); 
    r_line(1)=-l_down; 
    l_line(end)=-r_down; 
  
    % Injecting sound 
    if (k<=length(excited)) 

        r_line(1)=r_line(1)+excited(k); % insert excitation signal
    end 
%Recording sound 
sound_out(k)=r_line(end)+l_line(end); 
end

%%
fc = freq0;     % Set the cutoff frequency to the fundamental frequency
n = 3;                     % Filter order
Wn = (fc) / (fs/2);          % Normalize the cutoff frequency

% Design the Butterworth filter
[b, a] = butter(n, Wn);

%Apply the filter
sound_out = filter(b, a, sound_out);


%%


%%
% SIGNAL SCALING

% Step 2: Detect the Peak Amplitude
pk_amplitude = max(abs(sound_out));

% Step 3: Calculate the Desired Scaling Factor
desired_amplitude = max(abs(signal));  % desired amplitude
scaling_factor = desired_amplitude / pk_amplitude;

% Step 4: Scale the Signal
sound_out = sound_out * scaling_factor;

%%
% TRIM INITIAL DELAY & ADD TO LAST PART

% Trim leading zeros
start_index = find(sound_out, 1, 'first'); % Find the index of the first non-zero element
if isempty(start_index) % If no initial zeroes, retain original signal
    sound_out = sound_out;
else
    trimmed_signal = sound_out(start_index:end); % Trim the signal

    % Length to be filled
    original_length = length(sound_out);
    trimmed_length = length(trimmed_signal);
    fill_length = original_length - trimmed_length;

    % Replicate and reverse the last part of the trimmed signal to fill the length
    if fill_length > 0
        last_sound = trimmed_signal(end-min(fill_length, trimmed_length)+1:end); % Extract the last part
        last_sound = flipud(last_sound); % Reverse the last part
        replicated_part = repmat(last_sound, ceil(fill_length / length(last_sound)), 1); % Repeat the reversed part to match fill length
        replicated_part = replicated_part(1:fill_length); % Trim to exact fill length

        % Ensure both trimmed_signal and replicated_part have the same orientation
        if isrow(trimmed_signal)
            sound_out = [trimmed_signal, replicated_part']; % Concatenate as row vectors
        else
            sound_out = [trimmed_signal; replicated_part]; % Concatenate as column vectors
        end
    else
        sound_out = trimmed_signal;
    end
end
%%
% FILE SAVING AND PLOTTING

% Play the output sound
sound_out = sound_out * 1;
sound(sound_out, fs);
    
% Write the audio file
fullFilePath = fullfile('/Users/laustinrivar/Documents/UPD/5th - Second Sem (2023-2024)/ECE 199/DW Test/synth', finalsynthname);
%fullFilePath = fullfile('/Users/laustinrivar/Documents/UPD/5th - Second Sem (2023-2024)/ECE 199/DW Test', finalsynthname);
% audiowrite(fullFilePath, sound_out, fs);

% Plotting
t = (0:N-1) / fs;

figure;
subplot(3,1,1);
plot(t, signal);
title('Input Signal');
xlabel('Time (s)');
ylabel('Amplitude');

subplot(3,1,2);
plot(t, excited);
title('Input Signal');
xlabel('Time (s)');
ylabel('Amplitude');

subplot(3,1,3);
plot(t, sound_out);
title('Output Signal');
xlabel('Time (s)');
ylabel('Amplitude');

%%


audio1 = signal(:,1);
audio2 = sound_out(:,1);
        
        fft1 = abs(fft(audio1,44100));
        fft2 = abs(fft(audio2,44100));
        

        r = corrcoef(fft1,fft2);
aaacoef = r(2,1);
disp(aaacoef)


audio1 = signal(:,1);
audio2 = nexcited(:,1);
        
        fft1 = abs(fft(audio1,44100));
        fft2 = abs(fft(audio2,44100));

        r = corrcoef(fft1,fft2);
aaacoef1 = r(2,1);
disp(aaacoef)
