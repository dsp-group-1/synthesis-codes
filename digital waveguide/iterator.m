bestcoef =0;
bestz = 0;
a1list =[];
a1loc = [];
excited = nexcited;
for z = 1:300
try

% for i = 1:N
%     if i > 0.2*N
%         excited(i) = 0;
%     end
% end
Kr = 0.94;
delay_L = z;
%Wave array values from left to right 
r_line=zeros(1,delay_L); 
%Wave array values from right to left 
l_line=zeros(1,delay_L); 

for k=1:N 
  
    %Wave travel 
    r_down=r_line(end)*Kr; % ready to go into the lower rail 
    l_down=l_line(1)*Kr; % ready to go into the upper rail 
  
    %Air rotation 
    r_line=circshift(r_line,[1 1]); 
    l_line=circshift(l_line,[1 -1]); 
    r_line(1)=-l_down; 
    l_line(end)=-r_down; 
  
    % Injecting sound 
    if (k<=length(excited)) 

        r_line(1)=r_line(1)+excited(k); % insert excitation signal
    end 
%Recording sound 
sound_out(k)=r_line(end)+l_line(end); 
end

% Design the Butterworth filter
[b, a] = butter(n, Wn);

%Apply the filter
sound_out = filter(b, a, sound_out);
%%


%%
% SIGNAL SCALING

% Step 2: Detect the Peak Amplitude
pk_amplitude = max(abs(sound_out));

% Step 3: Calculate the Desired Scaling Factor
desired_amplitude = max(abs(signal));  % desired amplitude
scaling_factor = desired_amplitude / pk_amplitude;

% Step 4: Scale the Signal
sound_out = sound_out * scaling_factor;

%%
% TRIM INITIAL DELAY & ADD TO LAST PART

% Trim leading zeros
start_index = find(sound_out, 1, 'first'); % Find the index of the first non-zero element
if isempty(start_index) % If no initial zeroes, retain original signal
    sound_out = sound_out;
else
    trimmed_signal = sound_out(start_index:end); % Trim the signal

    % Length to be filled
    original_length = length(sound_out);
    trimmed_length = length(trimmed_signal);
    fill_length = original_length - trimmed_length;

    % Replicate and reverse the last part of the trimmed signal to fill the length
    if fill_length > 0
        last_sound = trimmed_signal(end-min(fill_length, trimmed_length)+1:end); % Extract the last part
        last_sound = flipud(last_sound); % Reverse the last part
        replicated_part = repmat(last_sound, ceil(fill_length / length(last_sound)), 1); % Repeat the reversed part to match fill length
        replicated_part = replicated_part(1:fill_length); % Trim to exact fill length

        % Ensure both trimmed_signal and replicated_part have the same orientation
        if isrow(trimmed_signal)
            sound_out = [trimmed_signal, replicated_part']; % Concatenate as row vectors
        else
            sound_out = [trimmed_signal; replicated_part]; % Concatenate as column vectors
        end
    else
        sound_out = trimmed_signal;
    end
end
%%
% FILE SAVING AND PLOTTING





%%


audio1 = signal(:,1);
audio2 = sound_out(:,1);
        
        fft1 = abs(fft(audio1,44100));
        fft2 = abs(fft(audio2,44100));

        r = corrcoef(fft1,fft2);
aaacoef = r(2,1);



audio1 = signal(:,1);
audio2 = nexcited(:,1);
        
        fft1 = abs(fft(audio1,44100));
        fft2 = abs(fft(audio2,44100));

        r = corrcoef(fft1,fft2);
aaacoef1 = r(2,1);

if bestcoef < aaacoef
    bestcoef = aaacoef;
    if bestcoef > 0.75
        a1list = [bestcoef, a1list];
        a1loc = [z, a1loc];
    end
    bestz = z;
end
end
end